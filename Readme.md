#### EKS Logging System to Cloudwatch Using FluentBit

##### Prerequirements:
a. Create a namespace 

b. Create a Customized fulentbit daemonset


##### Steps to Create namespace 

```
 cd /opt/fluentbit/
 kubectl apply -f https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/cloudwatch-namespace.yaml 
 kubectl apply -f cloudwatch-namespace.yaml
```
   

##### Steps to Create ConfigMap 

Replace cluster-name and cluster-region with your cluster's name and Region.
```
 ClusterName=cluster-name
RegionName=cluster-region
FluentBitHttpPort='2020'
FluentBitReadFromHead='Off'
[[ ${FluentBitReadFromHead} = 'On' ]] && FluentBitReadFromTail='Off'|| FluentBitReadFromTail='On'
[[ -z ${FluentBitHttpPort} ]] && FluentBitHttpServer='Off' || FluentBitHttpServer='On'
kubectl create configmap fluent-bit-cluster-info \
--from-literal=cluster.name=${ClusterName} \
--from-literal=http.server=${FluentBitHttpServer} \
--from-literal=http.port=${FluentBitHttpPort} \
--from-literal=read.head=${FluentBitReadFromHead} \
--from-literal=read.tail=${FluentBitReadFromTail} \
--from-literal=logs.region=${RegionName} -n amazon-cloudwatch
```

In this command, the FluentBitHttpServer for monitoring plugin metrics is on by default. To turn it off, change the third line in the command to FluentBitHttpPort='' (empty string) in the command.

Also by default, Fluent Bit reads log files from the tail, and will capture only new logs after it is deployed. If you want the opposite, set FluentBitReadFromHead='On' and it will collect all logs in the file system.


##### Steps to Create Customized fulentbit daemonset
```
 cd /opt/fluentbit/

wget https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/fluent-bit/fluent-bit-compatible.yaml
```
           
 By default log stream name show as Pod_name+contaner_id+Name_space($(tag[4])) We need only  Pod_name. For that we want to edit in
 ```
     vim /opt/fluentbit/new/fluent-bit-compatible.yaml

      [OUTPUT]
        Name                cloudwatch
        Match               application.*
        region              ${AWS_REGION}
        log_group_name      /aws/containerinsights/${CLUSTER_NAME}/application
        log_stream_name     $(kubernetes['pod_name'])
        auto_create_group   true
        extra_user_agent    container-insights 
 ```

 instead of (tag[4]) use this (kubernetes['pod_name'])

 
 ##### Apply the compatible.yaml file
`
    kubectl apply -f  fluent-bit-compatible.yaml
`

The above steps create the following resources in the cluster:

- A service account named Fluent-Bit in the amazon-cloudwatch namespace. This service account is used to run the Fluent Bit daemonSet. For more information, see Managing Service Accounts in the Kubernetes Reference.
- A cluster role named Fluent-Bit-role in the amazon-cloudwatch namespace. This cluster role grants get, list, and watch permissions on pod logs to the Fluent-Bit service account. For more information, see API Overview in the Kubernetes Reference.
- A ConfigMap named Fluent-Bit-config in the amazon-cloudwatch namespace. This ConfigMap contains the configuration to be used by Fluent Bit. For more information, see Configure a Pod to Use a ConfigMap in the Kubernetes Tasks documentation.

##### For Verify
```
 kubectl get pods -A
    3 pods will up in the same namespace
 login to the cloudwatch, and check
    3 log_groups will be there (Application,Dataplane,host)
```
